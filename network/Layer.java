package network;

import java.util.ArrayList;
import java.util.List;

import network.neurons.InputNeuron;


public abstract class Layer {
	
	protected List<Neuron> neurons = new ArrayList<Neuron>();
	
	private Layer next = null;
	
	private Layer previous = null;
	
	private String name = null;
	
	public Layer(String name)
	{
		this.setName(name);
		
		// create a bias neuron which always fires 1
		InputNeuron bias = new InputNeuron();
		bias.setLastValue(1.0);					

		// bias neuron is always added to a layer
		try {
			addNeuron(bias);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int numberOfNeurons() 
	{
		return neurons.size();
	}
	
	public void addNeuron(Neuron neuron) throws Exception
	{
		if(!compatibleWith(neuron))
			throw new Exception("incompatible type of neuron");
		
		neurons.add(neuron);
		
		// link layer to neuron
		neuron.setLayer(this);
	}
		
	public double[] getOutput(boolean useReadyData) throws Exception
	{
		double[] output = new double[numberOfNeurons()-1];
		
		for(int i = 1; i < numberOfNeurons(); i++)
			output[i-1] = neurons.get(i).activate(useReadyData);
		
		return output;
	}

	public void append(ProcessingLayer layer, boolean linkNeurons) throws Exception 
	{
		if (next != null)
			throw new Exception("layer is already appended to another layer");
		
		// connect layers
		next = layer;
		next.setPrevious(this);
		
		// link all neurons
		if(linkNeurons)
		{
			for(int n = 0; n < neurons.size(); n++)
			{
				for(int p = 1; p < layer.numberOfNeurons(); p++)
				{
					ProcessingNeuron nextNeuron = (ProcessingNeuron) layer.getNeuron(p);
					neurons.get(n).outputNeurons.add(nextNeuron);
					nextNeuron.addInputNeuron(neurons.get(n));
				}
			}
		}		
	}
	
	@SuppressWarnings("unchecked")
	private void checkIndex(int index, List list) throws Exception
	{
		if (index >= list.size())
			throw new Exception("invalid index");
	}
	
	public Neuron getNeuron(int index) throws Exception
	{
		checkIndex(index, neurons);
		return neurons.get(index);
	}
	
	public boolean isOutput() 
	{
		return getNext() == null;
	}
	
	public boolean isInput()
	{
		return getPrevious() == null;
	}

	public abstract boolean compatibleWith(Neuron neuron);

	public void setNext(Layer next) {
		this.next = next;
	}

	public Layer getNext() {
		return next;
	}

	public void setPrevious(Layer previous) {
		this.previous = previous;
	}

	public Layer getPrevious() {
		return previous;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Neuron> getNeurons() {
		return neurons;
	}

}
