package network;

import network.neurons.InputNeuron;


public class InputLayer extends Layer {
	
	public void setInput(double[] input) throws Exception
	{
		if (input.length != numberOfNeurons() - 1)
			throw new Exception("invalid input size");
			
		for(int i = 1; i < numberOfNeurons(); i++)
			((InputNeuron)neurons.get(i)).setLastValue(input[i-1]);			
	}
	
	@Override
	public boolean compatibleWith(Neuron neuron) 
	{
		return (neuron instanceof InputNeuron);
	}
	
	public InputLayer(String name)
	{
		super(name);
	}
}
