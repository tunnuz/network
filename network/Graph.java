package network;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.MediaTracker;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;


@SuppressWarnings("serial")
public class Graph extends Frame {

	BufferedImage image;
	
	
	public Graph(BufferedImage image, String title)
	{
		super(title);
		
		this.image = image;
		MediaTracker mt = new MediaTracker(this);
		mt.addImage(image,0);
		
		setSize(image.getWidth(), image.getHeight()+29);
		setVisible(true);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we) {
				dispose();
			}
		});

	}
	
	public void update(Graphics g){
		paint(g);
	}
		  
	public void paint(Graphics g){
		if(image != null)
			g.drawImage(image, 0, 29, this);
		else
			g.clearRect(0, 0, image.getWidth(), image.getHeight());
	}
	
}