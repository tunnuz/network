package network;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import java.util.Random;

import javax.imageio.ImageIO;

import network.learningFunctions.Backprop;
import network.neurons.IdentityNeuron;
import network.neurons.InputNeuron;
import network.neurons.LogisticNeuron;

public class Test {

	static BufferedImage out = null;

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		// input file
		BufferedImage avImage = ImageIO.read(new File("closeup.png"));
	
		Random random = new Random(System.nanoTime());
				
		// input layer
		InputLayer input = new InputLayer("input");
		input.addNeuron(new InputNeuron());
		input.addNeuron(new InputNeuron());
		
		ProcessingLayer hidden0 = new ProcessingLayer("hidden-0");
		for(int i = 0; i < 25; i++) 
			hidden0.addNeuron(new LogisticNeuron());
	
		ProcessingLayer hidden1 = new ProcessingLayer("hidden-1");
		for(int i = 0; i < 25; i++) 
			hidden1.addNeuron(new LogisticNeuron());
		
		ProcessingLayer hidden2 = new ProcessingLayer("hidden-1");
		for(int i = 0; i < 25; i++) 
			hidden2.addNeuron(new LogisticNeuron());
		
		// output layer
		ProcessingLayer output = new ProcessingLayer("output");
		output.addNeuron(new IdentityNeuron());		

		// output image
		out = new BufferedImage(200, 200, 1);
		Graph graph = new Graph(out, "Learned function");
		
		// link layers
		input.append(hidden0, true);
		hidden0.append(hidden1, true);
		hidden1.append(hidden2, true);
		hidden2.append(output, true);

		// network
		Backprop backprop = new Backprop();
		Network network = new Network(backprop);
		network.setInputLayer(input);
		
		// learning rate and decay
		backprop.setLearningRate(0.2);
		backprop.setLearningRateDecay(0.9999);
		
		// momentum & adaptive learning rate parameters
		backprop.setLearningRateChange(0.01);
		backprop.setErrorStepSize(0.1);
		backprop.setMomentum(0.0);
		
		double initialTime = System.currentTimeMillis();
		
		double graphTime = 10000;
		double lastGraphTime = System.currentTimeMillis();
		
		// Training
		System.out.println("Training ...");	
		
		double state;
		double action;
		Color col = null;
		
		while(random.nextDouble() != 1.1) //(System.currentTimeMillis() < time + timeLimit)
		{
			
			state = random.nextDouble()*avImage.getWidth();
			action = random.nextDouble()*avImage.getHeight();
			col = new Color(avImage.getRGB((int)state,(int)(avImage.getHeight()-action)));		// 9- is because of the way pixels are addressed in images
			
			network.train(
				new double[] { state/10.0, action/10.0 }, 			// in
				new double[] { col.getRed() / 255.0 } 				// out					
			);		
			
			
			// Update graph
			if (System.currentTimeMillis() > lastGraphTime + graphTime)
			{		
				

				lastGraphTime = System.currentTimeMillis();
				
				System.out.println("After "+ (lastGraphTime-initialTime)/1000.0 +" seconds ("+ backprop.getIterations() +" iterations), LR: " + backprop.getLearningRate());

				
				// Empty image
				for(int i = 0; i < out.getWidth(); i++)
					for(int j = 0; j < out.getHeight(); j++)
						out.setRGB(i,j,Color.WHITE.getRGB());
				
				
				// Repaint
				for(double x = -10.0; x < 10.0; x+= 0.1)
				{		
					for(double y = -10.0; y < 10.0; y+=0.1)
					{
						// axes
						show(out, x, 0, -10,10,-10,10, Color.GRAY);
						show(out, 0, y, -10,10,-10,10, Color.GRAY);
						
						// calculate input
						network.setInput(
							new double[]{ x, y }
						);
							
						// out
						double z = network.getOutput()[0];
												
						show(out, x, y, -10.0, 10.0, -10.0, 10.0, color(0, 1, clamp(0,1,z)));						
					}

				}
			
				graph.repaint();
			}

		}

		System.out.println();
		System.out.println("Final learning rate: " + backprop.getLearningRate());

		ImageIO.write(out, "png", new File("compressed_bwr.png"));

	}
	


	private static Color color(double minz, double maxz, double z) {
		float c = clamp(minz,maxz,(z / (maxz-minz)));
		return new Color(c,c,c);		
	}



	public static double toDouble(boolean a)
	{
		return a == false ? 0.0 : 1.0;
	}
	
	public static void show(BufferedImage image, double x, double y, double minX, double maxX, double minY, double maxY, Color color) throws Exception
	{
		// calculate input coordinate on x
		int xc = (int) clamp(-1, image.getWidth(), 	image.getWidth()/2 + (x / (maxX - minX) * image.getWidth()) );
		int yc = (int) clamp(-1, image.getHeight(), 	image.getHeight()/2 - ( y / (maxY - minY) * image.getHeight() ) );
				
		// set pixel in image
		if (xc != -1 && xc != image.getWidth() && yc != -1 && yc != image.getHeight())
			image.setRGB(xc, yc , color.getRGB() );
	}
	
	public static int rgbOf(Color color)
	{
		return color.getRGB();
	}
	
	public static float clamp(double minz, double maxz, double value)
	{
		
		if(value <= minz)
			return (float) minz;
		
		if(value >= maxz)
			return (float) maxz;
		
		return (float) value;
	}
}