package network;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public abstract class ProcessingNeuron extends Neuron {

	/**
	 * Pseudo-random number generator.
	 */
	static Random prng = new Random(System.currentTimeMillis());

	/**
	 * Last sum of weighted inputs.
	 */
	private double lastNet = 0;

	/**
	 * Last inputs.
	 */
	private List<Double> lastInput = new ArrayList<Double>();
	
	/**
	 * Connection weights.
	 */
	private List<Double> weights = new ArrayList<Double>();
	
	/**
	 * Input neurons.
	 */
	private List<Neuron> inputNeurons = new ArrayList<Neuron>();
	
	/**
	 * Add an input signal to this neuron.
	 * 
	 * @param neuron source of input.
	 * @throws Exception if the neuron is not an InputNeuron and we are in the first hidden layer.
	 */
	public void addInputNeuron(Neuron neuron) throws Exception 
	{
		// produce a random weight for this connection
		double randomWeight = prng.nextDouble() * 0.1 - 0.05;
		
		// don't allow duplicates
		if (inputNeurons.contains(neuron))
		{
			System.err.println("adding input neuron ... neuron already in the list of input neurons for this layer, discarding");
			return;
		}

		// otherwise link the two neurons
		inputNeurons.add(neuron);
		neuron.outputNeurons.add(this);
		
		// set weights
		weights.add(randomWeight);		// set small weight
		lastInput.add(0.0);

	}

	public Neuron getInputNeuron(int index)
	{
		return inputNeurons.get(index);
	}
	
	public ProcessingNeuron getOutputNeuron(int index)
	{
		return outputNeurons.get(index);
	}
	
	
	public void printWeights() 
	{
		System.out.print("weights: ");
		for(int i = 0; i < weights.size(); i++) 
		{
			if (i == 0)
				System.out.print("[b] ");
			else
				System.out.print("["+i+"] ");
			
			System.out.print(weights.get(i) + "\t");
		}
		System.out.println();
	}

	/**
	 * Propagate inputs.
	 * 
	 * @param useReadyData 	flag to control the activation of previous neurons during activation of the current one, 
	 * 						if set to false each input neuron is activated, otherwise already computed results are used
	 */
	private double propagate(boolean useReadyData)
	{
		// update lastNet and return it
		lastNet = 0;
		for(int i = 0; i < inputNeurons.size(); i++) 
		{
			if(useReadyData)
				setLastInput(i, getInputNeuron(i).getLastValue());
			else
				setLastInput(i, getInputNeuron(i).activate(false)); 		// store inputs
			
			lastNet += (getLastInput(i) * getWeight(i));					// sum inputs
		}
		return lastNet;
	}

	
	public double activate(boolean useReadyData)
	{
		// propagate signal from inputs and process it with neuron-specific activation function
		lastValue = activationFunction(propagate(useReadyData));
		return lastValue;
	}

	public int numberOfInputs() 
	{
		return inputNeurons.size();
	}
	
	public int numberOfOutputs() 
	{
		return outputNeurons.size();
	}
	
	
	public double getWeight(int index)
	{
		return weights.get(index);		
	}
	
	public void setWeight(int index, double newWeight)
	{
		weights.set(index, newWeight);		
	}
	
	public double getLastInput(int index)
	{
		return lastInput.get(index);		
	}
	
	public void setLastInput(int index, double newInput)
	{
		lastInput.set(index, newInput);		
	}

	public double getLastNet() 
	{
		return lastNet;
	}
	
	/**
	 * Neuron-specific activation function, to be implemented in subclasses.
	 * @throws Exception 
	 */
	public abstract double activationFunction(double net);
	
	
	/**
	 * First derivative of the activation function, if differentiable.
	 * @throws Exception 
	 * @throws Exception if not differentiable.
	 */
	public abstract double activationFunctionDerivative(double net) throws Exception;

}
