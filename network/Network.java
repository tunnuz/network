package network;

public class Network {

	/**
	 * Input layer.
	 */
	private InputLayer inputLayer = null;
	
	/**
	 * Output layer.
	 */
	private ProcessingLayer outputLayer = null;
	
	/**
	 * Algorithm used for learning.
	 */
	private LearningFunction learningFunction = null;
	
	/**
	 * Constructor.
	 * 
	 * @param inputNeurons an InputLayer for which already exists an output layer and optional hidden layers.
	 * @throws Exception if the input layer is null.
	 */
	public Network(LearningFunction learningFunction)
	{
		this.learningFunction = learningFunction;
	}
	
	public LearningFunction getLearningFunction()
	{
		return learningFunction;
	}
	
	public void setInputLayer(InputLayer inputLayer) throws Exception
	{
		// save pointer to input and get output
		this.inputLayer = inputLayer;
		this.outputLayer = getOutputLayer();		
	}
	
	/**
	 * Retrieves the output layer by following the layer chain.
	 * 
	 * @return the output layer.
	 * @throws Exception 
	 */
	public ProcessingLayer getOutputLayer() throws Exception
	{
		Layer output = inputLayer;
		
		// follow chain of layers
		while(output.getNext() != null)
			output = output.getNext();
		
		if (output != inputLayer)
			return (ProcessingLayer) output;
		else
			throw new Exception("input layer can't be an output layer");
	}
	
	public InputLayer getInputLayer()
	{
		return inputLayer;
	}
	
	public void setInput(double[] input) throws Exception
	{
		inputLayer.setInput(input);
	}
	
	public double[] getOutput() throws Exception
	{
		// process each layer
		Layer current = inputLayer;
		
		while(current.getNext() != null)
		{
			ProcessingLayer layer = (ProcessingLayer) current.getNext();
			
			for(int n = 1; n < layer.numberOfNeurons(); n++)
			{
				layer.getNeuron(n).activate(true);
			}
			
			current = current.getNext();
		}

		return outputLayer.getOutput(true);
	}
	
	public double[] getValueFor(double[] input) throws Exception
	{
		this.setInput(input);
		return this.getOutput();
	}

	public double train(double[] input, double[] output) throws Exception {
		return this.learningFunction.train(this, input, output);
	}
	
}
