package network;

public abstract class LearningFunction {

	public double train(Network network, double[] input, double[] output) throws Exception {
		return train(network, new double[][]{input}, new double[][]{output});	
	}
	public abstract double train(Network network, double[][] input, double[][] output) throws Exception;
	
}
