package network;

public class ProcessingLayer extends Layer {

	@Override
	public boolean compatibleWith(Neuron neuron) 
	{
		return true;
	}	
	
	public ProcessingLayer(String name)
	{
		super(name);
	}
}
