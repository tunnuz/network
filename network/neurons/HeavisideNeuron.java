package network.neurons;

import network.ProcessingNeuron;

public class HeavisideNeuron extends ProcessingNeuron {

	@Override
	public double activationFunction(double x) 
	{
		if (x > 0)
			return 1.0;
		else
			return 0.0;
	}

	@Override
	public double activationFunctionDerivative(double x) throws Exception 
	{
		throw new Exception("heaviside function is not differentiable");
	}

}
