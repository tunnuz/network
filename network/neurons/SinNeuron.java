package network.neurons;

import network.ProcessingNeuron;

public class SinNeuron extends ProcessingNeuron {

	@Override
	public double activationFunction(double net) {
		return Math.sin(net);
	}

	@Override
	public double activationFunctionDerivative(double net) {
		return Math.cos(net);
	}

}
