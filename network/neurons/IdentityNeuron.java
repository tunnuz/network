package network.neurons;

import network.ProcessingNeuron;

public class IdentityNeuron extends ProcessingNeuron {

	@Override
	public double activationFunction(double x) {
		return x;
	}

	@Override
	public double activationFunctionDerivative(double x) 
	{
		return 1;
	}
	
}
