package network.neurons;

import network.ProcessingNeuron;

public class LogisticNeuron extends ProcessingNeuron {

	private double primeDisplacement = 0.0;
	
	@Override
	public double activationFunction(double x)
	{
		return 1 / (1+ Math.exp(-x));
	}

	@Override
	public double activationFunctionDerivative(double x)
	{
		return primeDisplacement + activationFunction(x) * (1 - activationFunction(x));
	}

	public void setPrimeDisplacement(double primeDisplacement) {
		this.primeDisplacement = primeDisplacement;
	}

	public double getPrimeDisplacement() {
		return primeDisplacement;
	}

}
