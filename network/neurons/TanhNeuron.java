package network.neurons;

import network.ProcessingNeuron;

public class TanhNeuron extends ProcessingNeuron {

	@Override
	public double activationFunction(double x) 
	{
		return Math.tanh(x);
	}

	@Override
	public double activationFunctionDerivative(double x)
	{
		return 1 - Math.pow(Math.tanh(x),2);
	}

}
