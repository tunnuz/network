package network.learningFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import network.InputLayer;
import network.LearningFunction;
import network.Network;
import network.ProcessingLayer;
import network.ProcessingNeuron;

/**
 * 	IMPORTANT: THIS CLASS NEEDS TO BE FIXED
 * 
 * @author localtunnuz
 *
 */
public class Quickprop extends LearningFunction {
		
	private Map<ProcessingNeuron, List<Double>> lastDeltas = new HashMap<ProcessingNeuron, List<Double>>();
	private Map<ProcessingNeuron, List<Double>> errorDerivatives = new HashMap<ProcessingNeuron, List<Double>>();
	private Map<ProcessingNeuron, List<Double>> lastErrorDerivatives = new HashMap<ProcessingNeuron, List<Double>>();

	private int iterations;
	
	public double propagateError(Network network, double[] input, double[] output) throws Exception {
		
		// forward propagation
	 	network.setInput(input);
		double[] result = network.getOutput();
		
		// start from output layer
		ProcessingLayer outputLayer = network.getOutputLayer();
		InputLayer inputLayer = network.getInputLayer();
				
		// reset error
		double outputError = 0;
		
		// process output layer
		for(int o = 1; o < outputLayer.numberOfNeurons(); o++)
		{	
			ProcessingNeuron neuron = (ProcessingNeuron) outputLayer.getNeuron(o);
			
			// error derivatives bootstrap
			if (!errorDerivatives.containsKey(neuron)) {
				errorDerivatives.put(neuron, new ArrayList<Double>(neuron.numberOfInputs()));
				for( int i = 0; i < neuron.numberOfInputs(); i++)
					errorDerivatives.get(neuron).add(0.0);					
			}
				
			// calculate output neuron error (linear)
			neuron.setLastError((output[o-1] - result[o-1]));
			
			// calculate error derivative for each input neuron
			for(int i = 0; i < neuron.numberOfInputs(); i++)
				errorDerivatives.get(neuron).set(i, errorDerivatives.get(neuron).get(i) - (neuron.getLastError() * neuron.getLastInput(i)));

			outputError += neuron.getLastError();
		}
				
		// follow layer chain up to the input
		ProcessingLayer current = outputLayer;
		
		// do this until we get to input layer (which mustn't be trained)
		while( current.getPrevious() != inputLayer )
		{
			current = (ProcessingLayer) current.getPrevious();
			
			// update weights for each neuron
			for(int i = 1; i < current.numberOfNeurons(); i++)
			{
				ProcessingNeuron neuron = (ProcessingNeuron) current.getNeuron(i);
				
				// error derivatives bootstrap
				if (!errorDerivatives.containsKey(neuron)) {
					errorDerivatives.put(neuron, new ArrayList<Double>(neuron.numberOfInputs()));
					for( int p = 0; p < neuron.numberOfInputs(); p++)
						errorDerivatives.get(neuron).add(0.0);					
				}
				
				// calculate error from next neurons' errors and weights
				double error = 0;
				
				ProcessingNeuron nextNeuron = null;
				for(int n = 1; n < neuron.numberOfOutputs(); n++)
				{
					nextNeuron = neuron.getOutputNeuron(n);
					error += nextNeuron.getLastError() * nextNeuron.getWeight(i);
				}
				
				// multiply error for first derivative of activation(net)
				neuron.setLastError(error * neuron.activationFunctionDerivative(neuron.getLastNet()));
							
				// calculate error derivative for each input neuron
				for(int p = 0; p < neuron.numberOfInputs(); p++)
					errorDerivatives.get(neuron).set(p, errorDerivatives.get(neuron).get(p) - (neuron.getLastError() * neuron.getLastInput(p)));
								
			}			
		}
		
		return outputError;
	
	}

	public void updateWeights()
	{
		Iterator<Entry<ProcessingNeuron, List<Double>>> deltasIterator = errorDerivatives.entrySet().iterator();
	
		// iterate through all neurons
		while(deltasIterator.hasNext())
		{
			Entry<ProcessingNeuron, List<Double>> current = deltasIterator.next();
			ProcessingNeuron currentNeuron = current.getKey();
			List<Double> currentErrorDerivatives = current.getValue();
			
			// (first iteration)
			if (!lastDeltas.containsKey(currentNeuron)) {
				lastDeltas.put(currentNeuron, new ArrayList<Double>(currentNeuron.numberOfInputs()));
				for( int i = 0; i < currentNeuron.numberOfInputs(); i++)
					lastDeltas.get(currentNeuron).add(1.0);
			}
			
			// (first iteration)
			if (!lastErrorDerivatives.containsKey(currentNeuron)) {
				lastErrorDerivatives.put(currentNeuron, new ArrayList<Double>(currentNeuron.numberOfInputs()));
				for( int i = 0; i < currentNeuron.numberOfInputs(); i++)
					lastErrorDerivatives.get(currentNeuron).add(0.0);
			}
				
			List<Double> currentLastDeltas = lastDeltas.get(currentNeuron);
			List<Double> currentLastErrorDerivatives = lastErrorDerivatives.get(currentNeuron);

			double delta;
			
			// update weight for each input
			for(int i = 0; i < currentNeuron.numberOfInputs(); i++)
			{
				
				delta = currentLastDeltas.get(i) * currentErrorDerivatives.get(i) / ( currentLastErrorDerivatives.get(i) - currentErrorDerivatives.get(i) );
				
//				System.out.println("lastDelta: " + currentLastDeltas.get(i));
//				System.out.println("errorDerivative: " + currentErrorDerivatives.get(i));
//				System.out.println("lastErrorDerivative: " + currentLastErrorDerivatives.get(i));
				System.out.println("delta: " + delta);
				
 				currentNeuron.setWeight(i, currentNeuron.getWeight(i) + delta);
				
				// save last deltas clean deltas
				currentLastDeltas.set(i, delta);
				currentLastErrorDerivatives.set(i, currentErrorDerivatives.get(i));
				currentErrorDerivatives.set(i, 0.0);
			}			
		}
	}
	
	@Override
	public double train(Network network, double[][] input, double[][] output) throws Exception {
		
		double totalError = 0;
		
		// propagate error for each training instance (stores accumulated updates in deltas)
		for(int b = 0; b < input.length; b++) 
		{
			totalError += propagateError(network, input[b], output[b]);
			this.iterations++;
		}
		// update all the weights (copy deltas to lastDeltas)
		updateWeights();	

		return totalError;
	}

	public int getIterations() {
		return this.iterations;
	}
}
