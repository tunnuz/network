package network.learningFunctions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import network.InputLayer;
import network.LearningFunction;
import network.Network;
import network.ProcessingLayer;
import network.ProcessingNeuron;

/**
 * Standard backpropagation algorithm with optional adaptive learning rate and momentum.
 */
public class Backprop extends LearningFunction{

	private double learningRate = 0.1;
	private double learningRateChange= 0.001;
	private double errorStepSize = 0.0;
	private double learningRateDecay = 0.9999;
	private double momentum = 0.0;
	private double discountedError = 0.0;

	private Map<ProcessingNeuron, List<Double>> deltas = new HashMap<ProcessingNeuron, List<Double>>();
	private Map<ProcessingNeuron, List<Double>> lastDeltas = new HashMap<ProcessingNeuron, List<Double>>();
	
	private int iterations = 0;
	
	public double propagateError(Network network, double[] input, double[] output) throws Exception
	{
		// forward propagation
		network.setInput(input);
		double[] result = network.getOutput();

		// start from output layer
		ProcessingLayer outputLayer = network.getOutputLayer();
		InputLayer inputLayer = network.getInputLayer();
				
		// reset error
		double outputError = 0;
		
		// process output layer
		for(int o = 1; o < outputLayer.numberOfNeurons(); o++)
		{	
			ProcessingNeuron neuron = (ProcessingNeuron) outputLayer.getNeuron(o);
			
			// deltas bootstrap
			if (!deltas.containsKey(neuron)) {
				deltas.put(neuron, new ArrayList<Double>(neuron.numberOfInputs()));
				for( int i = 0; i < neuron.numberOfInputs(); i++)
					deltas.get(neuron).add(0.0);					
			}
				
			// calculate output neuron error (linear)
			neuron.setLastError((output[o-1] - result[o-1]));
			
			// calculate delta for each neuron
			for(int i = 0; i < neuron.numberOfInputs(); i++) 
			{
				// compute new delta and store it until weight update
				deltas.get(neuron).set(
					i,																				// weight for input i 
					deltas.get(neuron).get(i) + neuron.getLastError() * neuron.getLastInput(i)		// standard term (without momentum)
				);
			}
						
			outputError += neuron.getLastError();
		}
		
		// follow layer chain up to the input
		ProcessingLayer current = outputLayer;
		
		// do this until we get to input layer (which mustn't be trained)
		while( current.getPrevious() != inputLayer )
		{
			current = (ProcessingLayer) current.getPrevious();
			
			// update weights for each neuron
			for(int i = 1; i < current.numberOfNeurons(); i++)
			{
				ProcessingNeuron neuron = (ProcessingNeuron) current.getNeuron(i);
				
				// deltas bootstrap
				if (!deltas.containsKey(neuron)) {
					deltas.put(neuron, new ArrayList<Double>(neuron.numberOfInputs()));
					for( int j = 0; j < neuron.numberOfInputs(); j++)
						deltas.get(neuron).add(0.0);					
				}
				
				// calculate error from next neurons' errors and weights
				double error = 0;
				
				ProcessingNeuron nextNeuron = null;
				for(int n = 1; n < neuron.numberOfOutputs(); n++)
				{
					nextNeuron = neuron.getOutputNeuron(n);
					error += nextNeuron.getLastError() * nextNeuron.getWeight(i);
				}
				
				// multiply error for first derivative of activation(net)
				neuron.setLastError(error * (neuron.activationFunctionDerivative(neuron.getLastNet())));
												
				for(int p = 0; p < neuron.numberOfInputs(); p++) {
					
					// compute new delta and store it until weight update
					deltas.get(neuron).set(
						p,																	// weight for input p
						deltas.get(neuron).get(p) + neuron.getLastError() * neuron.getLastInput(p)						// standard term (without momentum)
					);
				}
			}
		}
		
		return outputError;
	}
	
	public void updateWeights()
	{
		Iterator<Entry<ProcessingNeuron, List<Double>>> deltasIterator = deltas.entrySet().iterator();
	
		// iterate through all neurons
		while(deltasIterator.hasNext())
		{
			Entry<ProcessingNeuron, List<Double>> current = deltasIterator.next();
			ProcessingNeuron currentNeuron = current.getKey();
			List<Double> currentDeltas = current.getValue();
			
			// (first iteration)
			if (!lastDeltas.containsKey(currentNeuron)) {
				lastDeltas.put(currentNeuron, new ArrayList<Double>(currentNeuron.numberOfInputs()));
				for( int i = 0; i < currentNeuron.numberOfInputs(); i++)
					lastDeltas.get(currentNeuron).add(0.0);
			}
				
			List<Double> currentLastDeltas = lastDeltas.get(currentNeuron);
			double delta = 0;
			
			// update weight for each input
			for(int i = 0; i < currentNeuron.numberOfInputs(); i++)
			{
				delta = currentDeltas.get(i);
				currentNeuron.setWeight(i, currentNeuron.getWeight(i) + getLearningRate() * currentDeltas.get(i) + getMomentum() * currentLastDeltas.get(i));
				
				// save last deltas clean deltas
				currentLastDeltas.set(i, delta);
				currentDeltas.set(i, 0.0);
			}			
		}
	}
	
	@Override
	public double train(Network network, double[][] input, double[][] output) throws Exception {
		
		double totalError = 0;
		
		// propagate error for each training instance (stores accumulated updates in deltas)
		for(int b = 0; b < input.length; b++) 
		{
			totalError += propagateError(network, input[b], output[b]);
			this.iterations++;
		}
		// update all the weights (copy deltas to lastDeltas)
		updateWeights();
		
		// update learning rate based on error and decay
		learningRate *= getLearningRateDecay();

		// update learning rate adaptively (if we're improving enlarge it, if we're deteriorating reduce it)
		if (Math.abs(totalError) < Math.abs(discountedError))
			setLearningRate(Math.min(1.0, learningRate + learningRateChange));
		else
			setLearningRate(Math.max(0.0, learningRate - learningRateChange * learningRate));
				
		// update recent error
		discountedError = errorStepSize * (totalError - discountedError);		

		return totalError;
	}
	
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}

	public double getLearningRate() {
		return learningRate;
	}

	public void setLearningRateChange(double learningRateChange) {
		this.learningRateChange = learningRateChange;
	}

	public double getLearningRateChange() {
		return learningRateChange;
	}

	public void setErrorStepSize(double errorStepSize) {
		this.errorStepSize = errorStepSize;
	}

	public double getErrorStepSize() {
		return errorStepSize;
	}

	public void setLearningRateDecay(double learningRateDecay) {
		this.learningRateDecay = learningRateDecay;
	}

	public double getLearningRateDecay() {
		return learningRateDecay;
	}

	public void setMomentum(double momentum) {
		this.momentum = momentum;
	}

	public double getMomentum() {
		return momentum;
	}

	public int getIterations() {
		return iterations;
	}	
}
