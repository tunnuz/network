package network;

import java.util.ArrayList;
import java.util.List;

public abstract class Neuron {
	
	/**
	 * Last net value.
	 */
	protected double lastValue = 0;
	
	/**
	 * Last error.
	 */
	protected double lastError = 0;

	/**
	 * Neuron's layer.
	 */
	protected Layer layer = null;

	public void setLayer(Layer layer) throws Exception
	{		
		this.layer = layer;
	}
	
	/**
	 * Output neurons.
	 */
	protected List<ProcessingNeuron> outputNeurons = new ArrayList<ProcessingNeuron>();
	
	
	/**
	 * Collects the inputs and pass them through a neuron-specific activation function.
	 * @return the output value.
	 */
	public abstract double activate(boolean useReadyData);
	
	/**
	 * Tells if this neuron is an output neuron.
	 * @return true if the neuron is contained in an output layer.
	 */
	public boolean isOutput()
	{
		return getLayer().isOutput();
	}

	public void setLastValue(double lastValue) {
		this.lastValue = lastValue;
	}

	public double getLastValue() {
		return lastValue;
	}

	public void setLastError(double lastError) {
		this.lastError = lastError;
	}

	public double getLastError() {
		return lastError;
	}

	public Layer getLayer() {
		return layer;
	}
}
